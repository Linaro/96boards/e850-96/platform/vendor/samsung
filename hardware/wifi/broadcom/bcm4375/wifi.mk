# Broadcom BCM4375 chipset

#@@ifneq ($(findstring device, $(wildcard vendor/samsung/hardware/wifi/broadcom/bcm4361/universal8895/device.mk)),)
include vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/device.mk
#@@endif

# For BCM4361
#@@ifneq ($(filter luge% great% baikal% cruiser% dream% dream2% hero2dream2% vkc% star% star2%, $(TARGET_PRODUCT)), )
#@@ 20190531 PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/.clminfo:vendor/etc/wifi/.clminfo
#@@ 20190531 PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcm4361_sta.bin_b2:vendor/etc/wifi/bcmdhd_sta.bin_b2
#@@ 20190531 PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcm4361_mfg.bin_b2:vendor/etc/wifi/bcmdhd_mfg.bin_b2
#@@PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcm4361_sta.bin_b2:vendor/etc/wifi/bcmdhd_sta.bin_b2
#@@PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcm4361_mfg.bin_b2:vendor/etc/wifi/bcmdhd_mfg.bin_b2
#@@endif

# For blob
#@@ 20190531 PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcmdhd_clm.blob_Star:vendor/etc/wifi/bcmdhd_clm.blob
#@@ifneq ($(filter luge% baikal% cruiser% dream% hero2dream2% vkc% , $(TARGET_PRODUCT)), )
#@@PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcmdhd_clm.blob_Dream:vendor/etc/wifi/bcmdhd_clm.blob
#@@endif
#@@ifneq ($(filter great%, $(TARGET_PRODUCT)), )
#@@PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcmdhd_clm.blob_Great:vendor/etc/wifi/bcmdhd_clm.blob
#@@endif
#@@ifneq ($(filter star%, $(TARGET_PRODUCT)), )
#@@PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4361/bcmdhd_clm.blob_Star:vendor/etc/wifi/bcmdhd_clm.blob
#@@endif

# For BCM4375
#@@ifneq ($(filter luge% great% baikal% cruiser% dream% dream2% hero2dream2% vkc% star% star2%, $(TARGET_PRODUCT)), )
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/.clminfo:vendor/etc/wifi/.clminfo
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_sta.bin_b1:vendor/etc/wifi/bcmdhd_sta.bin_b1
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_mfg.bin_b1:vendor/etc/wifi/bcmdhd_mfg.bin_b1
#@@ modify path : remove vendor
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/.clminfo:/etc/wifi/.clminfo
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_sta.bin_b1:/etc/wifi/bcmdhd_sta.bin_b1
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_sta.bin_b1:/etc/wifi/bcmdhd_sta.bin
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_mfg.bin_b1:/etc/wifi/bcmdhd_mfg.bin_b1

# For blob
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_clm.blob:vendor/etc/wifi/bcmdhd_clm.blob

#@@ modify path : remove vendor
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/bcmdhd_clm.blob:/etc/wifi/bcmdhd_clm.blob


# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wldu:vendor/bin/hw/wldu
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wldu.conf:vendor/etc/wifi/wldu.conf
#@@ifeq ($(TARGET_BUILD_VARIANT),eng)
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wl:vendor/bin/hw/wl
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/lspci:vendor/bin/hw/lspci
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/dhd:vendor/bin/hw/dhd
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wldu.conf:vendor/etc/wifi/wldu.conf
#@@endif

#@@ modify path : remove vendor
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wldu:/bin/hw/wldu
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wldu.conf:/etc/wifi/wldu.conf
#@@ifeq ($(TARGET_BUILD_VARIANT),eng)
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wl:/bin/hw/wl
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/lspci:/bin/hw/lspci
# PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/dhd:/bin/hw/dhd
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/wldu.conf:/etc/wifi/wldu.conf
#@@endif

PRODUCT_PACKAGES += dhd lspci wl wldu
PRODUCT_SOONG_NAMESPACES += vendor/samsung/hardware/wifi/broadcom/bcm4375
