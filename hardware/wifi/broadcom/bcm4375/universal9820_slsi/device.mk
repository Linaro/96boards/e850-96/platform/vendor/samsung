PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02j_b0:vendor/etc/wifi/nvram.txt_r02j_b0
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_murata_r031_b0:vendor/etc/wifi/nvram.txt_murata_r031_b0
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_murata_r032_b0:vendor/etc/wifi/nvram.txt_murata_r032_b0
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_murata_r033_b0:vendor/etc/wifi/nvram.txt_murata_r033_b0

PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02j_b2:vendor/etc/wifi/nvram.txt_r02j_b2
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02a_b2:vendor/etc/wifi/nvram.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02a_b2:vendor/etc/wifi/nvram_net.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02a_b2:vendor/etc/wifi/nvram.txt_r02a_b2

#PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:vendor/etc/wifi/nvram.txt_r02j_b2
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:vendor/etc/wifi/nvram.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:vendor/etc/wifi/nvram_net.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:vendor/etc/wifi/nvram.txt_CS01_semco_b1

# remove vendor
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02j_b0:/etc/wifi/nvram.txt_r02j_b0
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_murata_r031_b0:/etc/wifi/nvram.txt_murata_r031_b0
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_murata_r032_b0:/etc/wifi/nvram.txt_murata_r032_b0
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_murata_r033_b0:/etc/wifi/nvram.txt_murata_r033_b0

PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02j_b2:/etc/wifi/nvram.txt_r02j_b2
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02a_b2:/etc/wifi/nvram.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02a_b2:/etc/wifi/nvram_net.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_r02a_b2:/etc/wifi/nvram.txt_r02a_b2

#PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:vendor/etc/wifi/nvram.txt_r02j_b2
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:/etc/wifi/nvram.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:/etc/wifi/nvram_net.txt
PRODUCT_COPY_FILES += vendor/samsung/hardware/wifi/broadcom/bcm4375/universal9820_slsi/nvram.txt_CS01_semco_b1:/etc/wifi/nvram.txt_CS01_semco_b1


