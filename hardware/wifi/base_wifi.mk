# Wi-Fi Packages for Android
PRODUCT_PACKAGES += \
	wificond \
	libwifi-hal \
	libwifi-system \
	libwifikeystorehal \
	android.hardware.wifi.supplicant@1.2 \
	android.hardware.wifi@1.0 \
	android.hardware.wifi@1.0-service

# wpa_supplicant
PRODUCT_PACKAGES += \
	wpa_supplicant \
	wpa_supplicant.conf

# Support Samsung WLAN Macloader
PRODUCT_PACKAGES += \
	macloader

# wpa_supplicant_overlay.conf
PRODUCT_COPY_FILES += \
    vendor/samsung/hardware/wifi/config/p2p_supplicant_overlay.conf:vendor/etc/wifi/p2p_supplicant_overlay.conf
#    vendor/samsung/hardware/wifi/config/wpa_supplicant_overlay.conf:system/vendor/etc/wifi/wpa_supplicant_overlay.conf \
#    vendor/samsung/hardware/wifi/config/p2p_supplicant_overlay.conf:system/vendor/etc/wifi/p2p_supplicant_overlay.conf \

# init.wifi.rc
PRODUCT_COPY_FILES += \
       vendor/samsung/hardware/wifi/root/init.wifi.rc:vendor/etc/wifi/init.wifi.rc

$(warning #### [WIFI] WLAN_VENDOR = $(WLAN_VENDOR))
$(warning #### [WIFI] WLAN_CHIP = $(WLAN_CHIP))
$(warning #### [WIFI] WLAN_CHIP_TYPE = $(WLAN_CHIP_TYPE))
$(warning #### [WIFI] WIFI_NEED_CID = $(WIFI_NEED_CID))
$(warning #### [WIFI] ARGET_BOARD_PLATFORM = $(ARGET_BOARD_PLATFORM))
$(warning #### [WIFI] TARGET_BOOTLOADER_BOARD_NAME = $(TARGET_BOOTLOADER_BOARD_NAME))
